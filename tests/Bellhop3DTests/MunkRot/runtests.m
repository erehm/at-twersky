% run the 3D Munk case
% the angular limits are chosen so as not to hit the free surface
% check these closely if you see discrepancies

% The rotated case (Munk3D) is using walls to represent the top/bottom boundaries
% Since the bottom is all the same material we can't have different BC on
% the two walls

% Fine sampling is required to represent vertical walls well

global units
units = 'km';

%%
bellhop3d Munk3D

figure
plotshdpol( 'Munk3D.shd', 0, 0, 3000 )
caxisrev( [ 50 100 ] )
axis( [ -1 4 0 100 ] )

%%
bellhop3d Munk3Dz

figure
plotshd( 'Munk3Dz.shd' )
caxisrev( [ 50 100 ] )


%%
bellhop3d foo

figure
plotshdpol( 'foo.shd', 0, 0, 3000 )
caxisrev( [ 50 100 ] )
axis( [ -1 4 0 100 ] )

%%
bellhop3d fooz

figure
plotshd( 'fooz.shd' )
caxisrev( [ 50 100 ] )

%%
bellhop3d foozz

figure
plotshd( 'foozz.shd' )
caxisrev( [ 50 100 ] )

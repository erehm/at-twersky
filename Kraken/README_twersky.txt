Ice Scatter module
The Twersky-scatter option is problematic because it calls mathieu.f and slbessel.f which don't adhere to a rigorous Fortran standard. To avoid this problem, the current distribution has this option disabled.
If you want to put it back in, you will need to link to twersk.f instead of twerskfuse.f. These routines call mathieu.f which needs to be compiled with a switch to initialize all variables to zero. Most compilers have some such switch.

https://git.physics.byu.edu/dfvankom/acoustics-toolbox/blob/master/README_orig.md

-finit-local-zero is the gfortran option:

The -finit-local-zero option instructs the compiler to initialize local INTEGER, REAL, and COMPLEX variables to zero, LOGICAL variables to false, and CHARACTER variables to a string of null bytes. Finer-grained initialization options are provided by the -finit-integer=n, -finit-real=<zero|inf|-inf|nan|snan> (which also initializes the real and imaginary parts of local COMPLEX variables), -finit-logical=<true|false>, and -finit-character=n (where n is an ASCII character value) options.
https://gcc.gnu.org/onlinedocs/gfortran/Code-Gen-Options.html


